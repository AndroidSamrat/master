package com.stockbox.login.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import com.stockbox.common.SBParentActivity;
import com.stockbox.home.SB_Main_Activity;
import com.stockbox.login.event.GetUserInfo;
import com.stockbox.login.interactor.models.AccessDetails;
import com.stockbox.login.interactor.models.AccountHolder;
import com.stockbox.login.interactor.models.GetLogin;
import com.stockbox.login.interactor.models.UserDetails;
import com.stockbox.login.presenter.LogInPresenterImpl;
import com.stockbox.stockbox.R;
import com.stockbox.utility.Utils;


public class LogInActivity extends SBParentActivity implements View.OnClickListener, LogInView {

    private static final String TAG = "LogInActivity";
    private boolean rememberMe = false;
    private boolean showPwd = false;

    private ImageView splashIV, showPwdIV;
    private EditText loginEmailET, loginPwdET;
    private AppCompatButton loginActionBtn;
    private TextView loginForgotPwdTV,statusMsg;//errorMsgTV
    private LinearLayout loginLL;
    private RelativeLayout splashLL;
    private Switch keepSignedInSwitch;
    private LogInPresenterImpl logInPresenter;
    private ProgressBar progressBar;


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        logInPresenter = new LogInPresenterImpl();
        logInPresenter.setViewLogin(this);

        /*
         * Set the status bar color if required.
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.BlueGray));
        }

        loginLL = (LinearLayout) findViewById(R.id.loginLL);
        splashLL = (RelativeLayout) findViewById(R.id.splashLL);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        keepSignedInSwitch = (Switch) findViewById(R.id.keepSignedInSwitch);

        splashIV = (ImageView) findViewById(R.id.splashIV);
        showPwdIV = (ImageView) findViewById(R.id.showPwdIV);
        showPwdIV.setColorFilter(getResources().getColor(R.color.Green));
        loginEmailET = (EditText) findViewById(R.id.loginEmailET);
        loginPwdET = (EditText) findViewById(R.id.loginPwdET);
        loginActionBtn = (AppCompatButton) findViewById(R.id.loginActionBtn);
        loginForgotPwdTV = (TextView) findViewById(R.id.loginForgotPwdTV);
        //errorMsgTV = (TextView) findViewById(R.id.errorMsgTV);
        statusMsg = (TextView) findViewById(R.id.statusMsg);


        keepSignedInSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rememberMe = isChecked;
            }
        });

        loginForgotPwdTV.setOnClickListener(this);
        loginActionBtn.setOnClickListener(this);
        showPwdIV.setOnClickListener(this);

        launchSplashScreen();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        logInPresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        logInPresenter.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    private void launchLoginScreen() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                splashLL.setVisibility(View.GONE);
                loginLL.setVisibility(View.VISIBLE);
            }
        });
    }

    private void launchDashBoard() {
        Intent checkIn = new Intent(this, SB_Main_Activity.class);
        startActivity(checkIn);
        finish();
    }


    private void launchSplashScreen() {

        /*Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(DMSConstants.SPLASH_DURATION);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (Utils.isValidText(AccountHolder.getInstance().getUserName())) {
                        launchDashBoard();
                    } else {
                        launchLoginScreen();
                    }
                }
            }
        };
        timerThread.start();*/

        if (Utils.isValidText(AccountHolder.getInstance().getAccessToken())) {
            launchDashBoard();
        } else {
            launchLoginScreen();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void notifyEmailError() {
        loginEmailET.setError("Not a valid email");
        setClickListner(true);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void notifyPasswordError() {
        loginPwdET.setError("enter valid password");
        setClickListner(true);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void loginFailed(String errorMsg) {
        progressBar.setVisibility(View.GONE);
        statusMsg.setVisibility(View.GONE);
        //errorMsgTV.setText(errorMsg);
        //errorMsgTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void loginSuccessful(AccessDetails accessDetails) {
        /*statusMsg.setText("Loading UserInfo...");
        GetUserInfo getUserInfo = new GetUserInfo();
        getUserInfo.setAccessToken(accessDetails.getAccess_token());
        getUserInfo.setRememberUser(rememberMe);
        logInPresenter.getUserInfoFromServer(getUserInfo);*/
        statusMsg.setVisibility(View.GONE);
        Intent checkIn = new Intent(this, SB_Main_Activity.class);
        startActivity(checkIn);
        finish();
    }

    @Override
    public void validInput() {
        progressBar.setVisibility(View.VISIBLE);
        statusMsg.setVisibility(View.VISIBLE);
        statusMsg.setText("Loging In..");
        //errorMsgTV.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onSuccessfullyGotUserInfo(UserDetails userDetails) {
        statusMsg.setVisibility(View.GONE);
        Intent checkIn = new Intent(this, SB_Main_Activity.class);
        startActivity(checkIn);
        finish();
    }

    @Override
    public void onFailedUserInfo(String errorMsg) {
        progressBar.setVisibility(View.GONE);
        statusMsg.setVisibility(View.GONE);
        //errorMsgTV.setText(errorMsg);
        //errorMsgTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {

            case R.id.loginActionBtn:
                GetLogin getLogin = new GetLogin();
                getLogin.setT_username(loginEmailET.getText().toString());
                getLogin.setT_password(loginPwdET.getText().toString());
                getLogin.setRememberUser(rememberMe);
                logInPresenter.validateLoginData(getLogin);
                //errorMsgTV.setVisibility(View.INVISIBLE);

            break;

            case R.id.loginForgotPwdTV:


            break;

            case R.id.showPwdIV:
                if (Utils.isValidText(loginPwdET.getText().toString().trim())) {
                    if (!showPwd) {
                        showPwd = true;
                        showPwdIV.setColorFilter(getResources().getColor(R.color.Red));
                        loginPwdET.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        loginPwdET.setSelection(loginPwdET.length());
                    } else {
                        showPwd = false;
                        showPwdIV.setColorFilter(getResources().getColor(R.color.Green));
                        loginPwdET.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        loginPwdET.setSelection(loginPwdET.length());
                    }
                }
            break;
        }
    }


    public void setClickListner(boolean permision) {
        loginEmailET.setClickable(permision);
        loginPwdET.setClickable(permision);
        loginActionBtn.setClickable(permision);
        loginForgotPwdTV.setClickable(permision);
        keepSignedInSwitch.setClickable(permision);
    }
}
