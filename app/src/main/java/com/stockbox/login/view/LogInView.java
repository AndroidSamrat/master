package com.stockbox.login.view;


import com.stockbox.login.interactor.models.AccessDetails;
import com.stockbox.login.interactor.models.UserDetails;

public interface LogInView {

    void notifyEmailError();

    void notifyPasswordError();

    void loginFailed(String errorMsg);

    void loginSuccessful(AccessDetails accessDetails);

    void validInput();

    void onSuccessfullyGotUserInfo(UserDetails userDetails);

    void onFailedUserInfo(String errormsg);
}
