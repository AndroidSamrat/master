package com.stockbox.login.interactor.models;

import java.util.ArrayList;

/**
 * Created by biplabde on 8/10/16.
 */
public class UserDetails {

    public class InfoDetails{

        String user_id;
        String fname;
        String lname;
        String phone;
        String email;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }


    public class Role{
        String role_id;
        String role_name;

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getRole_name() {
            return role_name;
        }

        public void setRole_name(String role_name) {
            this.role_name = role_name;
        }
    }

    InfoDetails info;
    ArrayList<Role> roles;

    public InfoDetails getInfo() {
        return info;
    }

    public void setInfo(InfoDetails info) {
        this.info = info;
    }

    public ArrayList<Role> getRole() {
        return roles;
    }

    public void setRole(ArrayList<Role> roles) {
        this.roles = roles;
    }
}
