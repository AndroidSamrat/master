package com.stockbox.login.interactor.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by biplabde on 8/9/16.
 */
public class AccessDetails implements Parcelable {

    public AccessDetails(Parcel in){

    }


    public static final Creator<AccessDetails> CREATOR = new Creator<AccessDetails>() {
        @Override
        public AccessDetails createFromParcel(Parcel in) {
            return new AccessDetails(in);
        }

        @Override
        public AccessDetails[] newArray(int size) {
            return new AccessDetails[size];
        }
    };

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getStormpath_access_token_href() {
        return stormpath_access_token_href;
    }

    public void setStormpath_access_token_href(String stormpath_access_token_href) {
        this.stormpath_access_token_href = stormpath_access_token_href;
    }

    String token_type;
    String stormpath_access_token_href;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public String getExpire_time() {
        return expire_time;
    }

    public void setExpire_time(String expire_time) {
        this.expire_time = expire_time;
    }

    String id;
    String fname;
    String lname;
    String email;
    String phone;
    String profilePicture;
    String expires_in;
    String access_token;
    String refresh_token;
    String created_time;
    String expire_time;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
