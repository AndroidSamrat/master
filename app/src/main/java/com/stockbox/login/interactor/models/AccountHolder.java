package com.stockbox.login.interactor.models;

import android.app.Activity;
import android.content.SharedPreferences;

import com.stockbox.db.SBSharedPreference;
import com.stockbox.utility.Utils;

/**
 * Created by biplab on 9/8/16.
 * Holds all details related to the primary account
 */

public class AccountHolder {

    private final String PREF_IS_EMAIL_VERIFIED = "pref_is_email_verified";
    private final String PREF_USER_ID = "pref_user_id";
    private final String PREF_USER_NAME = "pref_user_name";
    private final String PREF_MOBILE_NO = "pref_mobile_no";
    private final String PREF_EMAIL = "pref_email";
    private final String PREF_ROLE = "pref_role";
    private final String PREF_ROLE_ID = "pref_role_id";
    private final String PREF_COOKIE = "pref_cookie";
    private final String PREF_ACCESS_TOKEN = "pref_acess_token";
    private final String PREF_REFRESH_TOKEN = "pref_refresh_token";
    private final String PREF_TOKEN_TYPE = "pref_token_type";
    private final String PREF_TOKEN_EXPIRE = "pref_expires_in";
    private final String PREF_ACCESS_TOKEN_HREF = "pref_stormpath_access_token_href";
    private final String PREF_TWILIO_ACCESS_TOKEN = "pref_twilio_access_token";


    //google registration
    public static final String GCM_REGISTRATION_ID = "GCM_REGISTRATION_ID";
    private static AccountHolder INSTANCE;
    private String userId;
    private String userName;
    private String mobileNo;
    private String email;
    private String role;
    private String role_id;
    private String cookie;
    private String accessToken;
    private String twilioAudioAccessToken;
    private String twilioVideoAccessToken;
    private String refreshToken;
    private String tokenType;
    private String tokenExpire;
    private String tokenHREF;
    private String gcmRegistrationId;



    public static AccountHolder getInstance() {
        if (null == INSTANCE) {
            INSTANCE = new AccountHolder();
        }
        return INSTANCE;
    }

    public void clear(Activity activity) {
        SharedPreferences preference = activity.getSharedPreferences(SBSharedPreference.ACCOUNT_PREFERENCE, 0);
        preference.edit().clear().apply();
        INSTANCE = null;
    }


    private AccountHolder() {
        userId = SBSharedPreference.getAccPreference(PREF_USER_ID);
        userName = SBSharedPreference.getAccPreference(PREF_USER_NAME);
        mobileNo = SBSharedPreference.getAccPreference(PREF_MOBILE_NO);
        email = SBSharedPreference.getAccPreference(PREF_EMAIL);
        role = SBSharedPreference.getAccPreference(PREF_ROLE);
        role_id = SBSharedPreference.getAccPreference(PREF_ROLE_ID);

        cookie = SBSharedPreference.getAccPreference(PREF_COOKIE);
        accessToken = SBSharedPreference.getAccPreference(PREF_ACCESS_TOKEN);

        //twilioAccessToken = DMSSharedPreference.getAccPreference(PREF_TWILIO_ACCESS_TOKEN);

        refreshToken = SBSharedPreference.getAccPreference(PREF_REFRESH_TOKEN);
        tokenType = SBSharedPreference.getAccPreference(PREF_TOKEN_TYPE);
        tokenExpire = SBSharedPreference.getAccPreference(PREF_TOKEN_EXPIRE);
        tokenHREF = SBSharedPreference.getAccPreference(PREF_ACCESS_TOKEN_HREF);
        gcmRegistrationId = SBSharedPreference.getAccPreference(GCM_REGISTRATION_ID);
    }


    public String getTwilioAudioAccessToken() {
        return twilioAudioAccessToken;
    }
    public String getTwilioVideoAccessToken() {
        return twilioVideoAccessToken;
    }

    public void setTwilioAudioAccessToken(String twilioAccessToken) {
        this.twilioAudioAccessToken = twilioAccessToken;
    }

    public void setTwilioVideoAccessToken(String twilioAccessToken) {
        this.twilioVideoAccessToken = twilioAccessToken;
    }



    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
        SBSharedPreference.storeAccPreferenceStringValue(PREF_ROLE, role);
    }



    public String getRoleId() {
        return role_id;
    }

    public void setRoleId(String roleid) {
        this.role_id = roleid;
        SBSharedPreference.storeAccPreferenceStringValue(PREF_ROLE_ID, roleid);
    }


    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
        SBSharedPreference.storeAccPreferenceStringValue(PREF_COOKIE, cookie);
    }


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String aToken) {
        this.accessToken = aToken;
        SBSharedPreference.storeAccPreferenceStringValue(PREF_ACCESS_TOKEN, aToken);
    }


    public String getGcmRegistrationId() {
        return gcmRegistrationId;
    }

    public void setGcmRegistrationId(String gcmRegistrationId) {
        this.gcmRegistrationId = gcmRegistrationId;
        SBSharedPreference.storeAccPreferenceStringValue(GCM_REGISTRATION_ID, gcmRegistrationId);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
        SBSharedPreference.storeAccPreferenceStringValue(PREF_USER_NAME, userName);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        SBSharedPreference.storeAccPreferenceStringValue(PREF_USER_ID, userId);
    }


    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo != null ? mobileNo : "";
        SBSharedPreference.storeAccPreferenceStringValue(PREF_MOBILE_NO, mobileNo);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        SBSharedPreference.storeAccPreferenceStringValue(PREF_EMAIL, email);
    }

    public boolean isSignedIn() {
        return Utils.isValidText(userId);
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getTokenExpire() {
        return tokenExpire;
    }

    public void setTokenExpire(String tokenExpire) {
        this.tokenExpire = tokenExpire;
    }

    public String getTokenHREF() {
        return tokenHREF;
    }

    public void setTokenHREF(String tokenHREF) {
        this.tokenHREF = tokenHREF;
    }
}
