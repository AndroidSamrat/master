package com.stockbox.login.interactor;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.stockbox.appcontroller.SBApp;
import com.stockbox.appcontroller.SBEventBus;
import com.stockbox.common.Interactor;
import com.stockbox.common.SBDisplayEvent;
import com.stockbox.common.SBToast;
import com.stockbox.login.event.GetUserInfo;
import com.stockbox.login.event.LoginStatus;
import com.stockbox.login.event.OnGotUserInfo;
import com.stockbox.login.interactor.models.AccessDetails;
import com.stockbox.login.interactor.models.AccountHolder;
import com.stockbox.login.interactor.models.GetLogin;
import com.stockbox.login.interactor.models.UserDetails;
import com.stockbox.network.ServerRequest;
import com.stockbox.network.ServerResponse;
import com.stockbox.network.Urls;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by biplabde on 8/2/16.
 */
public class LogInInteractorImpl implements Interactor {

    public static String TAG = "com.tekion.cdms.login.interactor.LogInInteractorImpl";
    private EventBus eventBus;


    public LogInInteractorImpl(){
        eventBus = SBEventBus.getEventBus();
    }

    /**
     * Implement and register with the event bus.
     */
    @Override
    public void onResume() {
        eventBus.register(this);
    }

    /**
     * Implement and unregister with the event bus.
     */
    @Override
    public void onPause() {
        eventBus.unregister(this);
    }


    @Subscribe
    public void onEventAsync(final GetLogin login) {

        ServerRequest request = new ServerRequest(Request.Method.POST, Urls.CDMS_LOGIN );
        request.addParam("username", login.getT_username());
        request.addParam("password", login.getT_password());

        request.execute(new ServerRequest.OnResponseListener() {
            @Override
            public void onResponse(ServerResponse serverResponse) {

                if (serverResponse.isSuccess()) {

                    AccessDetails accessDetails = (AccessDetails) serverResponse.getDataAsObject(AccessDetails.class);
                    LoginStatus status = new LoginStatus();
                    status.setAccessDetails(accessDetails);
                    status.setStatus(true);

                    if (login.isRememberUser()) {
                        AccountHolder.getInstance().setAccessToken(accessDetails.getAccess_token());
                        AccountHolder.getInstance().setRefreshToken(accessDetails.getRefresh_token());
                        AccountHolder.getInstance().setTokenExpire(accessDetails.getExpire_time());
                        AccountHolder.getInstance().setTokenType(accessDetails.getToken_type());
                        AccountHolder.getInstance().setTokenHREF(accessDetails.getStormpath_access_token_href());
                    }
                    eventBus.post(status);
                }
                else {
                    LoginStatus status = new LoginStatus();
                    status.setStatus(false);
                    status.setMsg(serverResponse.getErrorString());
                    eventBus.post(status);
                    eventBus.post(new SBDisplayEvent(serverResponse.getErrorString(), SBToast.LENGTH_CRITICAL));
                }
            }

            @Override
            public void onError(VolleyError error) {
                LoginStatus status = new LoginStatus();
                status.setStatus(false);
                status.setMsg(error.toString());
                eventBus.post(status);
            }
        }, request.getJsonObject(), true);
    }


    /*@Subscribe
    public void onEventAsync(final GetUserInfo getUserInfo) {

        ServerRequest request = new ServerRequest(Request.Method.GET, Urls.CDMS_GET_USER_INFO);
        request.addHeaderParam("tekion-api-token", getUserInfo.getAccessToken());

        request.execute(new ServerRequest.OnResponseListener() {
            @Override
            public void onResponse(ServerResponse serverResponse) {

                if (serverResponse.isSuccess()) {
                    OnGotUserInfo onGotUserInfo = new OnGotUserInfo();
                    UserDetails userDetails = (UserDetails)serverResponse.getDataAsObject(UserDetails.class);

                    if (getUserInfo.isRememberUser()) {
                        AccountHolder.getInstance().setUserName(userDetails.getInfo().getFname());
                        AccountHolder.getInstance().setEmail(userDetails.getInfo().getEmail());
                        AccountHolder.getInstance().setMobileNo(userDetails.getInfo().getPhone());
                        AccountHolder.getInstance().setUserId(userDetails.getInfo().getUser_id());
                        AccountHolder.getInstance().setRole(userDetails.getRole().get(0).getRole_name());
                        AccountHolder.getInstance().setRoleId(userDetails.getRole().get(0).getRole_id());
                    }
                    onGotUserInfo.setUserDetails(userDetails);
                    onGotUserInfo.setStatus(true);
                    eventBus.post(onGotUserInfo);
                }
                else {
                    OnGotUserInfo onGotUserInfo = new OnGotUserInfo();
                    onGotUserInfo.setStatus(false);
                    eventBus.post(onGotUserInfo);
                    eventBus.post(new SBDisplayEvent(serverResponse.getErrorString(), SBToast.LENGTH_NORMAL));
                }
            }

            @Override
            public void onError(VolleyError error) {
                OnGotUserInfo onGotUserInfo = new OnGotUserInfo();
                onGotUserInfo.setStatus(false);
                eventBus.post(onGotUserInfo);
            }
        }, request.getJsonObject(), true);
    }*/


}
