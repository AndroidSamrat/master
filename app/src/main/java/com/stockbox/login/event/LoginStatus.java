package com.stockbox.login.event;


import com.stockbox.login.interactor.models.AccessDetails;

public class LoginStatus {
    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    boolean status;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    String msg;

    public AccessDetails getAccessDetails() {
        return accessDetails;
    }

    public void setAccessDetails(AccessDetails accessDetails) {
        this.accessDetails = accessDetails;
    }

    AccessDetails accessDetails;
}
