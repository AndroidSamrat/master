package com.stockbox.login.event;

/**
 * Created by biplabde on 8/10/16.
 */
public class GetUserInfo {
    String accessToken;
    boolean rememberUser;

    public boolean isRememberUser() {
        return rememberUser;
    }

    public void setRememberUser(boolean rememberUser) {
        this.rememberUser = rememberUser;
    }



    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
