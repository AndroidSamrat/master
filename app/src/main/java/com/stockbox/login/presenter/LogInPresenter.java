package com.stockbox.login.presenter;


import com.stockbox.common.Presenter;
import com.stockbox.login.event.GetUserInfo;
import com.stockbox.login.interactor.models.GetLogin;
import com.stockbox.login.view.LogInView;

/**
 * Created by biplabde on 8/2/16.
 */
public interface LogInPresenter extends Presenter {

    void setViewLogin(LogInView loginView);

    void validateLoginData(GetLogin login);

    void getUserInfoFromServer(GetUserInfo getUserInfo);

}
