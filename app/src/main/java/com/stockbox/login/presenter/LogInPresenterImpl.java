package com.stockbox.login.presenter;


import com.stockbox.appcontroller.SBEventBus;
import com.stockbox.login.event.GetUserInfo;
import com.stockbox.login.event.LoginStatus;
import com.stockbox.login.event.OnGotUserInfo;
import com.stockbox.login.interactor.LogInInteractorImpl;
import com.stockbox.login.interactor.models.GetLogin;
import com.stockbox.login.view.LogInView;
import com.stockbox.utility.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class LogInPresenterImpl implements LogInPresenter {

    private LogInView loginView;
    private LogInInteractorImpl loginInteractor;
    private EventBus eventBus;

    public LogInPresenterImpl() {
        eventBus = SBEventBus.getEventBus();
        loginInteractor = new LogInInteractorImpl();
    }


    @Override
    public void validateLoginData(GetLogin login){

        boolean validated = true;

        if (!Utils.isValidText( login.getT_password())){
            validated = false;
            loginView.notifyPasswordError();
        }

        if (!Utils.isValidEmail(login.getT_username())){
            validated = false;
            loginView.notifyEmailError();
        }

        if (validated){
            loginView.validInput();
            login.setValidData(true);
            eventBus.post(login);
        }
    }

    @Override
    public void getUserInfoFromServer(GetUserInfo getUserInfo) {
        eventBus.post(getUserInfo);
    }


    @Override
    public void onResume() {
        eventBus.register(this);
        loginInteractor.onResume();
    }

    @Override
    public void onPause() {
        eventBus.unregister(this);
        loginInteractor.onPause();
    }

    @Override
    public void setViewLogin(LogInView loginView) {
        this.loginView = loginView;
    }


    /**
     * consume the event
     */
    @Subscribe
    public void onEventMainThread(LoginStatus status) {
        if (status.isStatus()){
            loginView.loginSuccessful(status.getAccessDetails());
        }
        else{
            loginView.loginFailed(status.getMsg());
        }
    }


    /**
     * consume the event
     */
    @Subscribe
    public void onEventMainThread(OnGotUserInfo onGotUserInfo) {
        if (onGotUserInfo.isStatus()){
            loginView.onSuccessfullyGotUserInfo(onGotUserInfo.getUserDetails());
        }
        else{
            loginView.onFailedUserInfo(onGotUserInfo.getMsg());
        }
    }

}
