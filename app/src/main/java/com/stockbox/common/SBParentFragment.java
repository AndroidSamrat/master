package com.stockbox.common;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.stockbox.stockbox.R;
import com.stockbox.common.*;

import java.util.List;

/**
 * Created by biplab on 29/7/16.
 * The parent for all the fragment_search screens in the app has to extend this to reuse the code
 */

public class SBParentFragment extends Fragment {

    protected View rootView = null;

    public SBParentFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        /* hide any toasts */
        SBToast.cancel();
    }

    /**
     * Used internally to get the FragmentManager for performing any transactions
     *
     * @return - Returns the FragmentManager from the Android Activity
     */
    private FragmentManager getSupportFragmentManager() {
        return getActivity().getSupportFragmentManager();
    }

    /**
     * Removes the view added through the fragment_search
     */
    protected void resetRootView() {
        if (rootView != null && rootView.getParent() != null) {
            ((ViewGroup) rootView.getParent()).removeView(rootView);
        }
    }

    /**
     * Adds a new screen to the backstack and replaces the current one on top
     *
     * @param fragment The fragment_search to be added to the backstack
     * @param tag The tag associated with the fragment_search, later can be used to find the fragment_search
     */
    public void addScreenInStack(Fragment fragment, String tag) {
        try {
//            if (tag == null) {
//                getSupportFragmentManager().beginTransaction().replace(R.id.fragmentcontainer, fragment).addToBackStack(null).commitAllowingStateLoss();
//            }
//            else {
//                getSupportFragmentManager().beginTransaction().replace(R.id.fragmentcontainer, fragment, tag).addToBackStack(null).commitAllowingStateLoss();
//            }
        }
        catch (IllegalStateException exception) {
            /* the activity might have been already destroyed */
        }
    }

    /**
     * Fnd the Fragment associated with the tag and remove it from the backstack and pop once.
     * Be careful, this method does not check if the fragment_search is at the top before popping.
     *
     * @param tag The tag for identifying the fragment_search.
     */
    protected void removeScreen(@NonNull String tag) {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(tag);
        if (fragment != null) {
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove(fragment);
            trans.commitAllowingStateLoss();
        }
        manager.popBackStack();
    }


    /**
     * Fnd the Fragment associated with the tag and remove it from the backstack and pop once.
     * Be careful, this method does not check if the fragment_search is at the top before popping.
     *
     * @param tag The tag for identifying the fragment_search.
     */
    protected void removeScreensUpto(@NonNull String tag) {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(tag);

        List<Fragment> fragments = manager.getFragments();
        int index = fragments.indexOf(fragment);

        for (int i = fragments.size()-1; i >index ; i--) {
            try {
                if(fragments.get(i)!=null && manager.findFragmentByTag(fragments.get(i).getTag()) !=null) {
                    manager.popBackStack();
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }



    /**
     * go back to previous fragment_search from the screen.
     */
    public void goBack() {
        try {
            FragmentManager manager = getSupportFragmentManager();
            if (manager.getFragments().size() > 0) {
                manager.popBackStack();
            }
        } catch (Exception ex) {
            /* not sure what to do */
        }
    }

    /**
     * Adds a new screen to the backstack and replaces the current one on top
     *
     * @param fragment The fragment_search to replace the current fragment_search at top of the stack
     * @param tag The tag associated with the fragment_search (later can be used to find the fragment_search)
     */
    protected void attachFragmentScreenToActivity(Fragment fragment, @NonNull String tag) {
//        getSupportFragmentManager()
//                .beginTransaction()
//                .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
//                .replace(R.id.fragmentcontainer, fragment, tag)
//                .addToBackStack(tag)
//                .commitAllowingStateLoss();
    }
}
