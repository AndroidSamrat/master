package com.stockbox.common;

/**
 * Created by biplab on 7/29/16.
 * For status messages
 */

public class SBDisplayEvent {
    private String message;
    private int duration;

    /**
     * Event for any critical errors or messages that needs to be displayed
     *
     * @param message The message to display.
     */
    public SBDisplayEvent(String message, int duration) {
        this.message = message;
        this.duration = duration;
    }

    public String getMessage() {
        return message;
    }

    public int getDuration() {
        return duration;
    }
}
