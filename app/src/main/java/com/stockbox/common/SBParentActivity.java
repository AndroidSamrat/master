package com.stockbox.common;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import com.stockbox.appcontroller.SBEventBus;
import com.stockbox.stockbox.R;
import com.stockbox.common.*;
import com.stockbox.common.SBToast;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class SBParentActivity extends AppCompatActivity {

    private EventBus eventBus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventBus = SBEventBus.getEventBus();

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        eventBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        eventBus.unregister(this);

    }

    @Override
    public void onStop() {
        super.onStop();
    }


    protected void attachFragment(Fragment fragment, @NonNull String tag,boolean removeAllPreviousFragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        //if (fragmentManager.findFragmentByTag(tag) == null) {
        if (removeAllPreviousFragment) {
            int count = getFragmentManager().getBackStackEntryCount();
            if (count > 0) {
                for (int i = count; i > 0; i--) {
                    try {
                        getSupportFragmentManager().popBackStack();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        fragmentManager.beginTransaction().replace(R.id.mainFragmentContainerFL, fragment, tag).addToBackStack(tag).commitAllowingStateLoss();
        //}
    }

    /**
     * consume the event CriticalDisplayEvent
     */
    @Subscribe
    public void onEventMainThread(CriticalDisplayEvent event) {
        SBToast.showText(this,event.message, SBToast.LENGTH_CRITICAL);
    }


    /**
     * consume the event Normal DisplayEvent
     */
    @Subscribe
    public void onEventMainThread(SBDisplayEvent event) {
        SBToast.showText(this,event.getMessage(),event.getDuration());
    }
}
