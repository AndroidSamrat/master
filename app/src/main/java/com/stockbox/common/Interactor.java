package com.stockbox.common;

/**
 * Created by biplab on 7/29/16.
 *
 * This is the *model* part of the MVP architecture, enforcing the implementation of lifecycle methods.
 * All others interact with the interactor through events.
 */
public interface Interactor {
    /**
     * Implement and register with the event bus.
     */
    void onResume();

    /**
     * Implement and unregister with the event bus.
     */
    void onPause();
}
