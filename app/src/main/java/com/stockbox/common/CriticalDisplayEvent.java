package com.stockbox.common;

/**
 * Created by biplab on 29/7/16.
 * This event will be fired while there is any authentication or criticle issue is there.
 */


public class CriticalDisplayEvent {
    public String message;

    /**
     * Event for any critical errors or messages that needs to be displayed
     *
     * @param message The message to display.
     */
    public CriticalDisplayEvent(String message) {
        this.message = message;
    }
}
