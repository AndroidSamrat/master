package com.stockbox.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.CountDownTimer;
import android.widget.Toast;

/**
 * Created by biplab on 29/7/16.
 */
public class SBToast {

    public static int LENGTH_NORMAL = 3000;
    public static int LENGTH_CRITICAL = 5000;
    public static int LENGTH_INFIITE = -1;

    /**
     * Keeps track of certain {@link SBToast} notifications that may need to be cancelled.
     * This functionality is only offered by some of the methods in this class.
     */
    private volatile static SBToast globalSBToast = null;

    /**
     * Internal reference to the {@link Toast} object that will be displayed.
     */
    private static Toast internalToast;
    private static CountDownTimer toastCountDown;

    private SBToast(Toast toast) {
        if (toast == null)
            throw new NullPointerException("SBToast.SBToast(Toast) requires a non-null parameter.");
        internalToast = toast;
    }

    @SuppressLint("ShowToast")
    private static SBToast makeText(Context context, CharSequence text, int duration) {
        return new SBToast(Toast.makeText(context, text, duration));
    }

    @SuppressLint("ShowToast")
    private static SBToast makeText(Context context, int resId, int duration)
            throws Resources.NotFoundException {
        return new SBToast(Toast.makeText(context, resId, duration));
    }

    public static void showText(Context context, CharSequence text, int duration) {
        if (text == null)
            cancel();
        else
            SBToast.makeText(context, text, Toast.LENGTH_SHORT).show(true, duration);
    }

    public static void showText(Context context, int resId, int duration)
            throws Resources.NotFoundException {
        SBToast.makeText(context, resId, Toast.LENGTH_SHORT).show(true, duration);
    }

    public static void cancel() {
        if (toastCountDown != null)
            toastCountDown.cancel();
        if (internalToast != null)
            internalToast.cancel();
    }

    public void show(boolean cancelCurrent, int duration) {
        // cancel current
        if (cancelCurrent && (globalSBToast != null)) {
            SBToast.cancel();
        }

        // save an instance of this current notification
        globalSBToast = this;
        showToast(duration);
    }

    public void showToast(int duration) {
        // Set the toast and duration
        int toastDurationInMilliSeconds = 30000;

        if (duration != -1)
            toastDurationInMilliSeconds = duration;

        // Set the countdown to display the toast
        toastCountDown = new CountDownTimer(toastDurationInMilliSeconds, 1000 /*Tick duration*/) {
            public void onTick(long millisUntilFinished) {
                internalToast.show();
            }

            public void onFinish() {
                SBToast.cancel();
            }
        };

        // Show the toast and starts the countdown
        internalToast.show();
        toastCountDown.start();
    }
}