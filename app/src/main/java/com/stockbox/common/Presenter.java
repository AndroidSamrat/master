package com.stockbox.common;

/**
 * Created by biplab on 7/29/16.
 *
 * This is the *presenter* component of the MVP architecture, enforcing the implementation of lifecycle methods.
 */
public interface Presenter {
    /**
     * Implement and register with the event bus.
     */
    void onResume();

    /**
     * Implement and unregister with the event bus.
     */
    void onPause();
}
