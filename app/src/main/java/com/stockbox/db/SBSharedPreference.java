package com.stockbox.db;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.google.gson.Gson;
import com.stockbox.appcontroller.SBApp;
import com.stockbox.appcontroller.SBConstants;
import com.stockbox.appcontroller.SBLogger;
import java.lang.reflect.Type;

public class SBSharedPreference {

	private static String TAG = "SBSharedPreference";

    public static String ACCOUNT_PREFERENCE = "account_pref";

	private static SBSharedPreference preferences = null;

	public static SBSharedPreference sharedPreference() {
		if (preferences == null) {
			SBLogger.showInfoLog("sharedPreference","initialize SBSharedPreference");
			preferences = new SBSharedPreference();
		}

		return preferences;
	}

	/*
	 * Method to return the SharedPreference based on the given key
	 * 
	 * @param String: Key to get the shared preference activity
	 * 
	 * @param Activity: activity to get the preferences
	 * 
	 * @return SharedPreferences: returns the SharedPreferences
	 */
	public static SharedPreferences getPreferencesFor(String key, Context context) {
		SBLogger.showInfoLog(TAG,"getPreferencesFor :: activity : " + context);
		if (context == null || key == null) {
			SBLogger.showInfoLog(TAG,"getPreferencesFor: Can not get the preference with null");
			return null;
		}
		SharedPreferences preference = context.getSharedPreferences(key, 0);
		if (preference == null) {
			SBLogger.showInfoLog(TAG,"There is no preferences for given key");
		}
		return preference;
	}

	public static Editor getEditor(String key) {
		SharedPreferences preference = SBApp.getInstance().getSharedPreferences(key, 0);
		if (preference == null) {
			SBLogger.showInfoLog(TAG,"There is no preferences for given key");
		}
		return preference.edit();
	}

	/**
	 * Clear all the shared pref data while logout or on authentication error
	 */
	public static void clearAllPreferences() {
		Editor accEditor = getEditor(ACCOUNT_PREFERENCE);
		accEditor.clear();
		accEditor.apply();

		SBApp.getInstance().getSharedPreferences("YOUR_PREFS", 0).edit().clear().apply();
	}


	/**
	 * Method to store the account preferences
	 * 
	 * @param key
	 * @param value
	 * @return none
	 */


	/*
	 * Store the string data to shared pref
	 */
	public static void storeAccPreferenceStringValue(String key, String value) {
		SBLogger.showDebugLog(TAG, "storeAccPreference");
		if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Account preferences can not be stored.");
			return;
		}
		if (value == null || value.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Value for given key is null or empty. Account preferences can not be stored.");
			return;
		}
		Editor editor = getEditor(ACCOUNT_PREFERENCE);
		editor.putString(key, value);
		editor.apply();

		SBLogger.showDebugLog(TAG,"Account Preference is stored successfully.");
	}

	public static void storeAccPreferenceIntValue(String key, int value) {
		SBLogger.showDebugLog(TAG, "storeAccPreference");
		if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Account preferences can not be stored.");
			return;
		}

		Editor editor = getEditor(ACCOUNT_PREFERENCE);
		editor.putInt(key, value);
		editor.apply();
		SBLogger.showDebugLog(TAG,"Account Preference is stored successfully.");
	}

	public static void storeAccPreferenceLongValue(String key, long value) {
		SBLogger.showDebugLog(TAG,"storeAccPreference");
		if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Account preferences can not be stored.");
			return;
		}

		Editor editor = getEditor(ACCOUNT_PREFERENCE);
		editor.putLong(key, value);
		editor.apply();
		SBLogger.showDebugLog(TAG,"Account Preference is stored successfully.");
	}

	public static void storeAccPreferenceboolValue(String key, boolean value) {
		SBLogger.showDebugLog(TAG, "storeAccPreference");
		if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Account preferences can not be stored.");
			return;
		}

		Editor editor = getEditor(ACCOUNT_PREFERENCE);
		editor.putBoolean(key, value);
		editor.apply();
		SBLogger.showDebugLog(TAG,"Account Preference is stored successfully.");
	}

    public static boolean getAccPreferenceBoolean(String key, boolean defaultValue, Activity con) {
		SBLogger.showDebugLog(TAG,"getAccPreferenceBoolean");
        SharedPreferences preference = getPreferencesFor(ACCOUNT_PREFERENCE,con);
        if (preference == null) {
			SBLogger.showDebugLog(TAG,"Account preference can not be loaded.");
            return defaultValue;
        }
        if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Account Preference can not be loaded.");
            return defaultValue;
        }
        if (!preference.contains(key)) {
			SBLogger.showDebugLog(TAG,"Key is not found in the Account preferences.");
            return defaultValue;
        }
        return preference.getBoolean(key, defaultValue);
    }


	public static void removeAccPreference(String key) {
		SBLogger.showDebugLog(TAG, "removeAccPreference");
		if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Account preferences can not be stored.");
			return;
		}

		Editor editor = getEditor(ACCOUNT_PREFERENCE);
		editor.remove(key);
		editor.apply();
		SBLogger.showDebugLog(TAG,"Account Preference removed successfully.");
	}

	/**
	 * Method to retrieve the account preferences
	 *
	 * @return String
	 */
	public static String getAccPreference(String key) {
		SBLogger.showDebugLog(TAG,"getAccPreference");
		SharedPreferences preference = getPreferencesFor(ACCOUNT_PREFERENCE, SBApp.getInstance());
		if (preference == null) {
			SBLogger.showDebugLog(TAG,"Account preference can not be loaded.");
			return SBConstants.EMPTY_STRING;
		}
		if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Account Preference can not be loaded.");
			return SBConstants.EMPTY_STRING;
		}
		if (!preference.contains(key)) {
			SBLogger.showDebugLog(TAG,"Key is not found in the Account preferences.");
			return SBConstants.EMPTY_STRING;
		}
		return preference.getString(key, SBConstants.EMPTY_STRING);
	}

	/**
	 * Method to retrieve the account preferences
	 *
	 * @return String
	 */
	public static Object getAccPreferenceObject(String key, Type typeOfT) {
		SBLogger.showDebugLog(TAG," getAccPreference");
		SharedPreferences preference = getPreferencesFor(ACCOUNT_PREFERENCE,SBApp.getInstance());
		if (preference == null) {
			SBLogger.showDebugLog(TAG,"Account preference can not be loaded.");
			return null;
		}
		if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Account Preference can not be loaded.");
			return null;
		}
		if (!preference.contains(key)) {
			SBLogger.showDebugLog(TAG,"Key is not found in the Account preferences.");
			return null;
		}
		String pref = preference.getString(key, SBConstants.EMPTY_STRING);
		if (!pref.equals(SBConstants.EMPTY_STRING)) {
			return new Gson().fromJson(pref, typeOfT);
		}
		else
			return null;
	}

	public static int getAccPreferenceInt(String key) {
		SBLogger.showDebugLog(TAG,"getAccPreference");
		SharedPreferences preference = getPreferencesFor(ACCOUNT_PREFERENCE, SBApp.getInstance());
		if (preference == null) {
			SBLogger.showDebugLog(TAG,"Account preference can not be loaded.");
			return 0;
		}
		if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Account Preference can not be loaded.");
			return 0;
		}
		if (!preference.contains(key)) {
			SBLogger.showDebugLog(TAG,"Key is not found in the Account preferences.");
			return 0;
		}
		return preference.getInt(key, 0);
	}

	public static long getAccPreferenceLong(String key) {
		SBLogger.showDebugLog(TAG,"BBSharedPreference :: getAccPreference");
		SharedPreferences preference = getPreferencesFor(ACCOUNT_PREFERENCE, SBApp.getInstance());
		if (preference == null) {
			SBLogger.showDebugLog(TAG,"Account preference can not be loaded.");
			return 0;
		}
		if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Account Preference can not be loaded.");
			return 0;
		}
		if (!preference.contains(key)) {
			SBLogger.showDebugLog(TAG,"Key is not found in the Account preferences.");
			return 0;
		}
		return preference.getLong(key, 0L);
	}

	public static long getTimeStamp(String key) {
		SBLogger.showDebugLog(TAG," getTimeStamp");
		SharedPreferences preference = getPreferencesFor(ACCOUNT_PREFERENCE, SBApp.getInstance());
		if (preference == null) {
			SBLogger.showDebugLog(TAG,"Account preference can not be loaded.");
			return 0;
		}
		if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Account Preference can not be loaded.");
			return 0;
		}
		if (!preference.contains(key)) {
			SBLogger.showDebugLog(TAG,"Key is not found in the Account preferences.");
			return 0;
		}
		return preference.getLong(key, 0L);
	}

	public static void storeTimeStamp(String key, long value) {
		SBLogger.showDebugLog(TAG,"storeTimeStamp");
		if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Time Stamp can not be stored.");
			return;
		}

		Editor editor = getEditor(ACCOUNT_PREFERENCE);
		editor.putLong(key, value);
		editor.apply();
		SBLogger.showDebugLog(TAG,"Time Stamp is stored successfully.");
	}

    public static boolean getAccPreferenceBoolean(String key, boolean defaultValue) {
		SBLogger.showDebugLog(TAG,"getAccPreferenceBoolean");
        SharedPreferences preference = getPreferencesFor(ACCOUNT_PREFERENCE, SBApp.getInstance());
        if (preference == null) {
			SBLogger.showDebugLog(TAG,"Account preference can not be loaded.");
            return defaultValue;
        }
        if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Account Preference can not be loaded.");
            return defaultValue;
        }
        if (!preference.contains(key)) {
			SBLogger.showDebugLog(TAG,"Key is not found in the Account preferences.");
            return defaultValue;
        }
        return preference.getBoolean(key, defaultValue);
    }

    public static void storeAccPreferenceBoolean(String key, Boolean value) {
		SBLogger.showDebugLog(TAG,"storeAccPreferenceBoolean");
        if (key == null || key.equalsIgnoreCase(SBConstants.EMPTY_STRING)) {
			SBLogger.showDebugLog(TAG,"Key is null or empty. Time Stamp can not be stored.");
            return;
        }

        Editor editor = getEditor(ACCOUNT_PREFERENCE);
        editor.putBoolean(key, value);
        editor.apply();
		SBLogger.showDebugLog(TAG,"Boolean is stored successfully.");
    }
}
