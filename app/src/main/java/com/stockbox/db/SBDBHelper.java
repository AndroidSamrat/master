package com.stockbox.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.stockbox.appcontroller.SBApp;
import com.stockbox.appcontroller.SBLogger;


/*
 * This class is singletone class for handling for database operation.
 */

public class SBDBHelper extends SQLiteOpenHelper {

    private static String TAG = "SBDBHelper";
    private static Context context;
    private static final String DATABASE_NAME = "cdms-db";
    private static SBDBHelper mInstance = null;
    private static int DATABASE_VERSION = 1;
    private static SQLiteDatabase database;


    /*
     * Define all the required table.
     */

    public static final String TABLE_USER_PROFILE = "user_profile";


    /*
     * Add all the tables name in array for easy to use where required.
     */
    private static final String[] TABLES = {TABLE_USER_PROFILE};


    /*
     * Define all the columns name here for each tables.
     */

    // columns for TABLE_USER_PROFILE table
    public static final String COLUMN_USER_ID = "user_id";


    /*
     * While updating version no define the columns here
     */


    /*
     * Define all the table defination here as string which will be executed later
     */

    private static final String CREATE_USER_PROFILE = "create table "
            + TABLE_USER_PROFILE + "(" + COLUMN_USER_ID + " text not null, " + " );";




    /*
     * constructor should be private to prevent direct instantiation. make call
     * to static factory method "getInstance()" instead.
     */

    protected SBDBHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
        context = ctx;
    }



    /*
     * This method used for getting for DB class singletone instance
     */

    public static SBDBHelper getInstance(Context ctx) {
        SBLogger.showErrorLog(TAG,"mInstance.." + mInstance);
        SBLogger.showErrorLog(TAG,"ctx.." + ctx);
        /**
         * use the application context as suggested by CommonsWare. this will
         * ensure that you don't accidentally leak an Activity context (see this
         * article for more information:
         * http://developer.android.com/resources/articles
         * /avoiding-memory-leaks.html)
         */
        if (mInstance == null) {
            if (ctx == null) { // safe check
                SBLogger.showInfoLog(TAG,"SBApp.getInstance()" + SBApp.getInstance());
                mInstance = new SBDBHelper(SBApp.getInstance());
            } else {
                SBLogger.showInfoLog(TAG,"ctx.getApplicationContext()" + ctx.getApplicationContext());
                mInstance = new SBDBHelper(ctx.getApplicationContext());
            }
        }
        SBLogger.showErrorLog(TAG,"mInstance.." + mInstance);
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_USER_PROFILE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

       // Write some logic to upgrade the db.

    }

    /**
     * Method to clear data from all table
     */
    public void clearTables() {
        SBLogger.showInfoLog(TAG,"CDMSDBHelper :: clearDB");
        SQLiteDatabase database = getDatabase();
        if (database != null && database.isOpen()) {
            for (String table : TABLES) {
                database.delete(table, null, null);
            }
        } else {
            SBLogger.showInfoLog(TAG,"Unable to clear the records in table.");
        }
    }

    public static SQLiteDatabase getDatabase() {
        if (database == null) {
            database = SBDBHelper.getInstance(context).getWritableDatabase();
        }

        return database;
    }

    public void closeDatabase() {
        if (database != null && database.isOpen()) {
            database.close();
            database = null;
        }
    }


}
