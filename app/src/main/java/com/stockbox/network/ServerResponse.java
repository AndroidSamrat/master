package com.stockbox.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import java.lang.reflect.Type;

/**
 * Created by biplabde on 8/2/16.
 * <p/>
 * Holds the response from the cdms server.
 */

public class ServerResponse {
    /**
     * The response from the server.
     */
    JSONObject response = null;
    String responseString = null;

    /**
     * Parses the response and creates a {@link JSONObject}. Expects the response always to be in JSON format.
     *
     * @param response The response from the server
     */
    public ServerResponse(String response) {
        try {
            this.responseString = response;
            this.response = new JSONObject(response);
        } catch (Exception e) {
            /* something went wrong */
        }
    }

    /**
     * Creates a new instance with the JSON response.
     *
     * @param response The response from the server
     */
    public ServerResponse(JSONObject response) {
        this.response = response;
        Log.d("Biplab","Response = "+response.toString());
    }

    /**
     * Checks the status code of the response
     *
     * @return true if the status is 200/201/202, false otherwise
     */
    public boolean isSuccess() {
        try {
            return (this.response.getJSONObject("meta").getInt("code") == 200
                    |this.response.getJSONObject("meta").getInt("code") == 201
                    |this.response.getJSONObject("meta").getInt("code") == 202);
        } catch (Exception e) {
            return false;
        }
    }



    /**
     * Checks the status code of the response
     *
     * @return true if the status is 11011, invalid ref code;
     */
    public boolean isInvalidRefCode() {
        try {
            return this.response.getInt("code") == 11011;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * @return returns the status code in the response
     */
    @SuppressWarnings("unused")
    public int getStatusCode() {
        try {
            return this.response.getJSONObject("meta").getInt("code");
        } catch (Exception e) {
            return -1;
        }
    }


    /**
     * @return data portion of the response
     */
    public String getDataString() {
        try {
            return this.response.getString("data");
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * @return entire String portion of the response
     */
    public String getWholeString() {
        try {
            return this.responseString;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @return data portion of the response
     */
    public String getErrorString() {
        try {
            return this.response.getJSONObject("meta").getString("msg");
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @return data portion of the response
     */
    public JSONObject getData() {
        try {
            return this.response.getJSONObject("data");
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @return data portion of the response
     */
    public JSONArray getDataArray() {
        try {
            return this.response.getJSONArray("data");
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Converts the data portion of the response into an object of specified type
     *
     * @param typeOfT The type to which the data portion of the response should be converted to
     * @return the converted object
     */
    public <T> T getDataAsType(Type typeOfT) {
        try {
            return new Gson().fromJson(this.response.getString("data"), typeOfT);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Converts the data portion of the response into an object of specified type
     *
     * @param typeOfT The type to which the data portion of the response should be converted to
     * @return the converted object
     */
    public <T> T getDataAsType(Type typeOfT, TypeAdapterFactory typeAdapterFactory) {
        try {
            return new GsonBuilder()
                    .registerTypeAdapterFactory(typeAdapterFactory)
                    .create().fromJson(this.response.getString("data"), typeOfT);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Converts the data portion of the response into an object of specified type
     *
     * @param typeOfT The type to which the data portion of the response should be converted to
     * @return the converted object
     */
    public <T> T getDataAsType(Type typeOfT, Type typeOfAdapter, TypeAdapter typeAdapter) {
        try {
            return new GsonBuilder()
                    .registerTypeAdapter(typeOfAdapter, typeAdapter)
                    .create().fromJson(this.response.getString("data"), typeOfT);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Converts the data portion of the response into an object of specified class
     *
     * @param classOfT The class to which the data portion of the response should be converted to
     * @return The data as an instance of classOfT
     */
    public Object getDataAsObject(Class classOfT) {
        try {
            return new Gson().fromJson(this.response.getString("data"), classOfT);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * In some specific cases, the data portion is not a JSON. It is just a long value
     *
     * @return The long value in the data portion
     */
    public long getLong() {
        try {
            return this.response.getLong("data");
        } catch (Exception e) {
            return -1L;
        }
    }
}
