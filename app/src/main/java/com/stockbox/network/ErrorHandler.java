package com.stockbox.network;

import android.content.Context;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.stockbox.appcontroller.SBEventBus;
import com.stockbox.common.CriticalDisplayEvent;
import com.stockbox.common.SessionExpiredEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by biplabde on 1/8/16.
 * <p/>
 * Handles errors that are caused by network and unknown server errors.
 */

public class ErrorHandler {

    /**
     * Handles standard network errors such as
     * {@link AuthFailureError}, {@link NetworkError}, {@link NoConnectionError}, {@link TimeoutError}
     * and 500 (internal server error ) and parse errors
     * In case of authentication errors, the user is logged out and the database is cleaned.
     * For other errors, an error message is displayed to the user.
     * <p/>
     * {@link ServerError} and {@link ParseError}
     *
     * @param error The occurred error
     * @param ctx   Activity Context
     */

    public static void handle(VolleyError error, Context ctx) {

        EventBus eventBus = SBEventBus.getEventBus();

        if (error instanceof AuthFailureError) {
            eventBus.postSticky(new SessionExpiredEvent());
        } else if (error instanceof NetworkError || error instanceof TimeoutError) {
            /* slow or no connection */
            eventBus.postSticky(new CriticalDisplayEvent("Network Error. Unable to connect or connection timed out."));
        } else if (error instanceof ParseError || error instanceof ServerError) {
            /* issues with our server */
            eventBus.postSticky(new CriticalDisplayEvent("Something went wrong on our end. Please accept our apologies."));
        } else {
            eventBus.postSticky(new CriticalDisplayEvent(error.getMessage()));
        }
    }

    public static boolean handle(ServerResponse response) {
        EventBus eventBus = SBEventBus.getEventBus();

        /* Check for error type if it is auth failure then fire a event to take some action

        if (response.getStatusCode() == ErrorCode.SESSION_EXPIRED) {
            eventBus.postSticky(new SessionExpiredEvent());
            return true;
        }*/

        return false;
    }
}
