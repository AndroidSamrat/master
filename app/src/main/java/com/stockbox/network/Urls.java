package com.stockbox.network;


import com.stockbox.appcontroller.SBApp;


public class Urls {

    public static final String API_VERSION = "1.0";

    public final static String SB_SEND_CODE = "http://localhost:8080/scan/ASF35276FAS";

    public final static String CDMS_LOGIN = SBApp.environment.getServerUrl() + "login";

    public final static String CDMS_GET_USER_INFO = SBApp.environment.getServerUrl() + "user/info";

    public final static String CDMS_GET_CONTACTS = SBApp.environment.getServerUrl() + "user/contacts";

    public final static String CDMS_GET_CUSTOMER = SBApp.environment.getServerUrl() + "user/customercontacts";

    public static final String TOKEN_SERVICE_URL = "http://ec2-54-183-73-33.us-west-1.compute.amazonaws.com:5099/get_communication_tokens";

    public static final String SMS_SERVICE_URL = "http://ec2-54-183-73-33.us-west-1.compute.amazonaws.com:5099/send_sms";



}
