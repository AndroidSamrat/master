package com.stockbox.network;

import android.os.Build;
import android.util.Log;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.stockbox.appcontroller.SBApp;
import com.stockbox.appcontroller.SBLogger;
import com.stockbox.network.*;
import com.stockbox.network.Urls;
import com.stockbox.utility.Utils;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by biplabde on 02/08/16.
 * <p/>
 * Handles requests to the server.
 * Given a URL, method and parameters it adds all the necessary headers, cookies, etc. to the request
 * and uses Volley to execute the request.
 */

public class ServerRequest {

    private String TAG = "ServerRequest";
    protected Map<String, String> param = new HashMap<>();
    protected Map<String, String> headerParam = new HashMap<>();

    /**
     * HTTP Method {@link Request.Method}
     */
    protected int method;

    /**
     * URL for invocation
     */
    protected String url;

    /**
     * Creates a new request.
     *
     * @param method One of {@link Request.Method}
     * @param url    The URL to be invoked
     */
    public ServerRequest(int method, String url) {
        this.method = method;
        this.url = url;
    }

    /**
     * Adds a new parameter (key, value pair) to the request
     *
     * @param key   The parameter name
     * @param value The parameter value
     */
    public void addParam(String key, String value) {
        if (param != null)
            param.put(key, value);
    }


    /**
     * Adds a new header param (key, value pair) to the request
     *
     * @param key   The parameter name
     * @param value The parameter value
     */
    public void addHeaderParam(String key, String value) {
        if (headerParam != null)
            headerParam.put(key, value);
    }



    /**
     * Get added param as a json object
     */
    public JSONObject getJsonObject() {
        if (param != null)
            return new JSONObject(param);
        else
            return null;
    }


    /**
     * Executes the request and invokes the appropriate listener upon response.
     *
     * @param listener    The listener into invoke upon response from the server.
     * @param jsonObject  json payload
     * @param cookie Set to true if cookie needs to be sent along with the request
     */
    public void execute(final OnResponseListener listener, JSONObject jsonObject, final boolean cookie) {
        RequestQueue queue = SBApp.getRequestQueue();
        SBLogger.showDebugLog("ServerRequest", "URL:" + url);

        JsonObjectRequest request = new JsonObjectRequest(method, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                SBLogger.showDebugLog("Response", "on response");
                listener.onResponse(new ServerResponse(response));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorHandler.handle(error, null);
                listener.onError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return param;
            }

            @Override
            public Map<String, String> getHeaders() {
                //HashMap<String, String> headers = new HashMap<>();
                //headers.put("Client-OS", "ANDROID");
                //headers.put("Client-OS-Version", Build.VERSION.RELEASE);
                //headers.put("API-VERSION", Urls.API_VERSION);
                return headerParam;
            }
        };

        int RETRIES = 0;
        int TIMEOUT = 30000;
        request.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT, RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);
        queue.add(request);
    }

    /**
     * Executes the request and invokes the appropriate listener upon response.
     *
     * @param listener      The listener into invoke upon response from the server.
     * @param tParseHeaders Set to true if the header needs to be parsed for cookie and csrf
     * @param cookie   Set to true if cookie needs to be sent along with the request
     * @param cache        Set to true if the response should be cached
     */
    public void execute(final OnResponseListener listener, final boolean tParseHeaders,final boolean cookie, boolean cache) {
        /* handle the parameters for a get request */
        if (method == Request.Method.GET && param.size() > 0) {
            String params = "";
            for (Map.Entry<String, String> entry : param.entrySet()) {
                params += (params.equals("") ? "" : "&") + Utils.encodeParam(entry.getKey()) + "=" + Utils.encodeParam(entry.getValue());
            }
            url += "?" + params;
        }

        RequestQueue queue = SBApp.getRequestQueue();
        SBLogger.showDebugLog(TAG, "URL:" + url);
        StringRequest request = new StringRequest(method, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String strResponse) {
                SBLogger.showDebugLog(TAG,"Response:"+strResponse);
                ServerResponse response = new ServerResponse(strResponse);
                if (response.isSuccess())
                    listener.onResponse(response);
                else {
                    if (!ErrorHandler.handle(response))
                        listener.onResponse(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorHandler.handle(error, null);
                listener.onError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return param;
            }

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                //headers.put("Client-OS", "ANDROID");
                //headers.put("Client-OS-Version", Build.VERSION.RELEASE);
                //headers.put("App-Version", Utils.getAppVersion());
                //headers.put("API-VERSION", Urls.API_VERSION);
                return headers;
            }



            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse networkResponse) {
                try {
                    if (tParseHeaders && (networkResponse.statusCode == 200||networkResponse.statusCode == 201)) {
                        String sessionId = "", csrfToken = null;

                        for (String key : networkResponse.headers.keySet()) {
                            if (key.equals("csrftoken")) {
                                csrfToken = networkResponse.headers.get(key);
                            } else if (key.equals("sessionid")) {
                                sessionId = networkResponse.headers.get(key);
                            }
                        }

                        if (csrfToken != null && !csrfToken.equals("")) {
                        /*AccountHolder.getInstance().setCSRFToken(csrfToken);
                        if (sessionId != null && !sessionId.equals("")) {
                            String cookie = Constants.LOGIN_CSRFTOKEN + "=" + csrfToken + " " + Constants.LOGIN_SESSION_ID + "=" + sessionId;
                            AccountHolder.getInstance().setCookie(cookie);

                        }*/
                        }
                    }
                    return super.parseNetworkResponse(networkResponse);
                }
                catch (Exception e) {
                    return Response.error(new ParseError(e));
                }
            }
        };

        int RETRIES = 0;
        int TIMEOUT = 30000;
        request.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT, RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(cache);
        queue.add(request);
    }

    public ServerResponse executeSync() {
        /* handle the parameters for a get request */
        if (method == Request.Method.GET && param.size() > 0) {
            String params = "";
            for (Map.Entry<String, String> entry : param.entrySet()) {
                params += (params.equals("") ? "" : "&") + Utils.encodeParam(entry.getKey()) + "=" + Utils.encodeParam(entry.getValue());
            }
            url += "?" + params;
        }

        RequestQueue queue = SBApp.getRequestQueue();
        SBLogger.showDebugLog(TAG, "URL:" + url);


        final RequestFuture<String> future = RequestFuture.newFuture();


        StringRequest request = new StringRequest(method, url, future, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorHandler.handle(error, null);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return param;
            }

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Client-OS", "ANDROID");
                headers.put("Client-OS-Version", Build.VERSION.RELEASE);
                headers.put("API-VERSION", Urls.API_VERSION);
                return headers;
            }
        };

        int RETRIES = 0;
        int TIMEOUT = 30000;
        request.setRetryPolicy(new DefaultRetryPolicy(TIMEOUT, RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);
        queue.add(request);
        try {
            return new ServerResponse(future.get(30, TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            Log.e(TAG, "Call interrupted.");
        } catch (ExecutionException e) {
            Log.e(TAG, "Execution Exception");
        } catch (TimeoutException e) {
            Log.e(TAG, "Timed out");
        }
        return null;
    }

    /**
     * Implement this interface to handle responses from the SBApp server
     */
    public interface OnResponseListener {
        void onResponse(ServerResponse serverResponse);

        void onError(VolleyError error);
    }
}
