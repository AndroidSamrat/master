package com.stockbox.scanner.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;
import com.stockbox.common.SBToast;
import com.stockbox.stockbox.R;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class Scanner_Activity extends AppCompatActivity implements ZXingScannerView.ResultHandler,View.OnClickListener {

    private String TAG = "Scanner_Activity";
    private ZXingScannerView mScannerView;
    private RelativeLayout scannerRL;
    private LayoutInflater layoutInflater;
    private TextView value;
    private ProgressBar showprogress;
    private Button send;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_scanner);

        scannerRL = (RelativeLayout)findViewById(R.id.scannerRL);
        value = (TextView) findViewById(R.id.codeValue);
        showprogress = (ProgressBar) findViewById(R.id.showprogress);
        showprogress.setVisibility(View.GONE);
        send = (Button)findViewById(R.id.sendBtn);

        mScannerView = new ZXingScannerView(this);
        scannerRL.addView(mScannerView);

        send.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        //Log.v(TAG, rawResult.getText()); // Prints scan results
        //Log.v(TAG, rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        mScannerView.resumeCameraPreview(this);
        value.setText(rawResult.getText());
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.sendBtn:
                Intent intent = this.getIntent();
                intent.putExtra("BAR_CODE",value.getText().toString() );
                this.setResult(RESULT_OK, intent);
                finish();
            break;
        }
    }
}
