package com.stockbox.appcontroller;

import android.util.Log;



public class SBLogger {

    /*
     * his is for when bad stuff happens. Use this tag in places like inside a catch statment.
     * You know that an error has occurred and therefore you're logging an error.
     */
    public static void showErrorLog(String TAG,String msg){
        if (SBConstants.SHOW_LOG) {
            Log.e(TAG, msg);
        }
    }



    /*
     *  Use this when you want to go absolutely nuts with your logging.
     *  If for some reason you've decided to log every little thing in a particular part of
     *  your app,use the Log.v tag.
     */
    public static void showVerboseLog(String TAG,String msg){
        if (SBConstants.SHOW_LOG) {
            Log.v(TAG, msg);
        }
    }


    /*
     * Use this to post useful information to the log.
     * For example: that you have successfully connected to a server.
     * Basically use it to report successes.
     */

    public static void showInfoLog(String TAG,String msg){
        if (SBConstants.SHOW_LOG) {
            Log.i(TAG, msg);
        }
    }


    /*
     * Use this for debugging purposes. If you want to print out a bunch of messages
     * so you can log the exact flow of your program, use this. If you want to keep a log of
     * variable values, use this.
     */

    public static void showDebugLog(String TAG,String msg){
        if (SBConstants.SHOW_LOG) {
            Log.d(TAG, msg);
        }
    }


    /*
     * Use this when you suspect something shady is going on. You may not be completely in
     * full on error mode, but maybe you recovered from some unexpected behavior.
     * Basically, use this to log stuff you didn't expect to happen but isn't necessarily an error.
      * Kind of like a "hey, this happened, and it's weird, we should look into it.
     */


    public static void showWarnLog(String TAG,String msg){
        if (SBConstants.SHOW_LOG) {
            Log.d(TAG, msg);
        }
    }


    /*
     * Use this when stuff goes absolutely, horribly, holy-crap wrong. You know those catch blocks
     * where you're catching errors that you never should get...yea,
     * if you wanna log them use Log.wtf
     */

    public static void showWTFailure(String TAG,String msg){
        if (SBConstants.SHOW_LOG) {
            Log.wtf(TAG, msg);
        }
    }


}
