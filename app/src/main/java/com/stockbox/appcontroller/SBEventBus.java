package com.stockbox.appcontroller;


import org.greenrobot.eventbus.EventBus;

/**
 * Created by biplabde on 1/18/16.
 * Creates a singleton
 */
public class SBEventBus {

    private static volatile EventBus eventBus;

    private SBEventBus() {
    }

    static void init() {
        eventBus = new EventBus();
    }

    public static EventBus getEventBus() {
        return eventBus;
    }
}
