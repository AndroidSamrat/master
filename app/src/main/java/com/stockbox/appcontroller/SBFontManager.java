package com.stockbox.appcontroller;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by biplabde on 2/19/16.
 * To manage the fonts - specifically cache them.
 */

public class SBFontManager {

    private static SBFontManager INSTANCE;

    /**
     * The Typeface cache, keyed by their asset file name.
     */
    private static final Map<String, Typeface> mCache = new HashMap<>();

    /**
     * Current context
     */
    private final Context mContext;

    /**
     * Returns the singleton instance of the PixlUIfaceManager.
     *
     * @return The PixlUIfaceManager
     */
    public static SBFontManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new SBFontManager(context);
        }
        return INSTANCE;
    }

    /**
     * Initializes the typeface manager with the given xml font file.
     *
     * @param context A context with which the xml font file and the assets can be accessed.
     */
    private SBFontManager(Context context) {
        mContext = context;
    }

    /**
     * Returns the typeface identified by the given font name. The font must be a name
     * defined in any nameset in the font xml file.
     *
     * @param name font file name including the extension
     * @return the typeface identified by the given font name
     */
    public Typeface getTypeface(String name) {
        synchronized (mCache) {
            if (!mCache.containsKey(name)) {
                Typeface t = Typeface.createFromAsset(mContext.getAssets(), String.format("fonts/%s", name));
                mCache.put(name, t);
                return t;
            } else {
                return mCache.get(name);
            }
        }
    }
}
