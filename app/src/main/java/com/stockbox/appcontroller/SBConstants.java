package com.stockbox.appcontroller;

/**
 * Created by biplabde on 7/29/16.
 */

public class SBConstants {
    public static boolean SHOW_LOG = true; // Control the showing log
    public static int SPLASH_DURATION = 5000;
    public static final String EMPTY_STRING = "";
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static int GET_BARCODE = 1;
}
