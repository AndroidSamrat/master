package com.stockbox.appcontroller;

/**
 * Created by biplab on 8/2/16.
 */
public enum SBEnvironment {

    DEVELOPMENT {
        @Override
        public String getServerUrl() {
            return "http://api.tekion.xyz/";
        }
    },

    STAGING {
        @Override
        public String getServerUrl() {
            return "";
        }
    },

    PRODUCTION {
        @Override
        public String getServerUrl() {
            return "";
        }

    };

    public abstract String getServerUrl();
}
