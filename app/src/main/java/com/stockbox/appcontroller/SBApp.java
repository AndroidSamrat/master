package com.stockbox.appcontroller;

import android.app.Application;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.stockbox.appcontroller.SBEnvironment;
import com.stockbox.appcontroller.SBEventBus;
import com.stockbox.appcontroller.SBFontManager;

import org.greenrobot.eventbus.Subscribe;


public class SBApp extends Application {

    private static final String TAG = "SBApp";

    /* switch to the right api SBEnvironment */
    public static SBEnvironment environment = SBEnvironment.DEVELOPMENT;

    private static SBApp sbInstance = null;
    private static RequestQueue queue = null; // volley request queue

    @Override
    public void onCreate() {
        super.onCreate();
        sbInstance = this;

        if (queue == null) {
            queue = Volley.newRequestQueue(this);
        }

        SBFontManager.getInstance(this);
        SBEventBus.init();
        SBEventBus.getEventBus().register(this);
    }


    public static synchronized SBApp getInstance() {

        return sbInstance;
    }


    public static RequestQueue getRequestQueue() {
        return queue;
    }




    @Subscribe
    public void onEventMainThread(String event) {

    }
}
