package com.stockbox.utility;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.stockbox.appcontroller.SBApp;
import com.stockbox.appcontroller.SBConstants;
import com.stockbox.appcontroller.SBLogger;


import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by biplabde on 8/2/16.
 */

public class Utils {

    private static float xFactor = -1;
    private static String appVersion = null;

    public static void buttonStyle(Button button) {
        button.setTextColor(Color.parseColor("#FFFFFF"));
        button.setBackgroundColor(Color.parseColor("#2089d6"));
    }

    public static int dpToPx(int dp) {
        DisplayMetrics displayMetrics = SBApp.getInstance().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static void openPlayStore(Context context) {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public static void displayToast(Context context, String toastMsg) {
        if (context == null || toastMsg == null || toastMsg.equals(SBConstants.EMPTY_STRING)) {
            SBLogger.showDebugLog("Utils", "displayToast :: Can not display toast.");
            return;
        }

        final Toast toast = Toast.makeText(context, toastMsg, Toast.LENGTH_SHORT);
        toast.show();

        android.os.Handler handler = new android.os.Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 1500);
    }


    public static long getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    public static long getPostDate(int delay) {
        long currentDate = getCurrentDate();

        long gracePeriod = (24 * 60 * 60 * 1000L) * delay;

        return currentDate + gracePeriod;
    }

    public static long getPreDate(int pre) {
        long currentDate = getCurrentDate();

        long gracePeriod = (24 * 60 * 60 * 1000L) * pre;

        return currentDate - gracePeriod;
    }


    public static long getForwardCalculatedDate(long date, int forward) {
        long gracePeriod = (24 * 60 * 60 * 1000L) * forward;
        return date + gracePeriod;
    }

    public static long getBackwordCalculatedDate(long date, int forward) {
        long gracePeriod = (24 * 60 * 60 * 1000L) * forward;
        return date - gracePeriod;
    }

    public static boolean checkDateInBoundary(long startdate, long endDate, long inputDate) {
        if (inputDate > startdate && inputDate < endDate)
            return true;
        else
            return false;
    }


    private static int elapsed(GregorianCalendar before, GregorianCalendar after, int field) {
        GregorianCalendar clone = (GregorianCalendar) before.clone();
        int elapsed = -1;
        while (!clone.after(after)) {
            clone.add(field, 1);
            elapsed++;
        }
        return elapsed;
    }


    public static Drawable resizeImage(Context context, int image, int newHeight, int newWidth) {
        return new BitmapDrawable(context.getResources(),
                Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                        context.getResources(), image), dpToPixels(newWidth), dpToPixels(newHeight), true));
    }

    public static void init(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int valueInPx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, metrics);
        xFactor = (float) valueInPx / 100;
    }

    public static int dpToPixels(int dpValue) {
        return (int) (dpValue * xFactor);
    }

    public static int spToPixels(Context context, float sp) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (sp * scaledDensity);
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow((null == activity
                .getCurrentFocus()) ? null : activity.getCurrentFocus()
                .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void hideMyKeyboard(Activity activity) {
        if (null == activity || null == activity.getCurrentFocus()) {
            return;
        }
        View view = activity.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String toString(ArrayList<String> stringArray) {
        String returnStr = "";
        for (String str : stringArray)
            returnStr += (returnStr.equals("")) ? str : ", " + str;
        return returnStr;
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static int suggestedItemWidth(Activity activity, int minWidthInDP,
                                         int noOfItems) {
        int screenWidth;
        minWidthInDP = dpToPixels(minWidthInDP);
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
            screenWidth = size.x;
        } else {
            screenWidth = display.getWidth();
        }

        return Math.max(screenWidth / (screenWidth / minWidthInDP), screenWidth / noOfItems);
    }

    public static Animation inFromRightAnimation() {

        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(700);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    public static Animation inFromTopAnimation() {

        Animation inFromBottom = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -0.1f,
                Animation.RELATIVE_TO_PARENT, +0.0f);
        inFromBottom.setDuration(700);

        inFromBottom.setInterpolator(new AccelerateInterpolator());
        return inFromBottom;
    }

    public static Animation inFromBottomAnimation(long duration) {

        Animation inFromBottom = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromBottom.setDuration(duration);

        inFromBottom.setInterpolator(new AccelerateInterpolator());
        return inFromBottom;
    }


    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }

    public static String getMessageDigestmd5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Get the network info
     *
     * @param context - Android context
     * @return - NetworkInfo object
     */
    private static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static boolean isConnected(Context context) {
        if (context == null)
            return false;
        NetworkInfo info = Utils.getNetworkInfo(context);
        return (info != null && info.isConnected());
    }




    public static String getAppVersion() {
        Context ctx = SBApp.getInstance();
        if (appVersion == null) {
            try {
                PackageInfo pinfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
                appVersion = pinfo.versionName;
            } catch (NameNotFoundException e) {
                SBLogger.showErrorLog("AppVersion", " get app version number : " + e);
                appVersion = "UNKNOWN";
            }
        }
        return appVersion;
    }


    public static String removeWhiteSpace(String key) {
        StringBuilder result = new StringBuilder("");
        for (int i = 0; i < key.length(); i++) {
            if (key.charAt(i) != ' ') {
                result.append(key.charAt(i));
            }
        }
        return result.toString();
    }


    public static boolean launchGoogleMaps(Context context, double latitude, double longitude, String label) {
        try {
            if (!isPackageInstalled("com.google.android.apps.maps", context))
                return false;

            String format = "geo:0,0?q=" + Double.toString(latitude) + "," + Double.toString(longitude) + "(" + label + ")";
            Uri uri = Uri.parse(format);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            intent.setPackage("com.google.android.apps.maps");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        } catch (ActivityNotFoundException exception) {
            return false;
        }
        return true;
    }

    private static boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (NameNotFoundException e) {
            return false;
        }
    }

    public static CharSequence filterForCity(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

        if (source instanceof SpannableStringBuilder) {
            SpannableStringBuilder sourceAsSpannableBuilder = (SpannableStringBuilder) source;
            for (int i = end - 1; i >= start; i--) {
                char currentChar = source.charAt(i);
                if (!Character.isLetter(currentChar) && !Character.isSpaceChar(currentChar)) {
                    sourceAsSpannableBuilder.delete(i, i + 1);
                }
            }
            return source;
        } else {
            StringBuilder filteredStringBuilder = new StringBuilder();
            for (int i = start; i < end; i++) {
                char currentChar = source.charAt(i);
                if (Character.isLetter(currentChar) || Character.isSpaceChar(currentChar)) {
                    filteredStringBuilder.append(currentChar);
                }
            }
            return filteredStringBuilder.toString();
        }
    }

    public static CharSequence filterForName(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

        if (source instanceof SpannableStringBuilder) {
            SpannableStringBuilder sourceAsSpannableBuilder = (SpannableStringBuilder) source;
            for (int i = end - 1; i >= start; i--) {
                char currentChar = source.charAt(i);
                if (!Character.isLetter(currentChar) && !Character.isSpaceChar(currentChar) && !(currentChar == '.')) {
                    sourceAsSpannableBuilder.delete(i, i + 1);
                }
            }
            return source;
        } else {
            StringBuilder filteredStringBuilder = new StringBuilder();
            for (int i = start; i < end; i++) {
                char currentChar = source.charAt(i);
                if (Character.isLetter(currentChar) || Character.isSpaceChar(currentChar) || currentChar == '.') {
                    filteredStringBuilder.append(currentChar);
                }
            }
            return filteredStringBuilder.toString();
        }
    }

    public static CharSequence nameCapFilter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

        if (source instanceof SpannableStringBuilder) {
            SpannableStringBuilder sourceAsSpannableBuilder = (SpannableStringBuilder) source;
            for (int i = end - 1; i >= start; i--) {
                char currentChar = source.charAt(i);
                if (!Character.isLetter(currentChar) && !Character.isSpaceChar(currentChar)) {
                    sourceAsSpannableBuilder.delete(i, i + 1);
                }
            }
            return source;
        } else {
            StringBuilder filteredStringBuilder = new StringBuilder();
            for (int i = start; i < end; i++) {
                char currentChar = source.charAt(i);
                if (Character.isLetter(currentChar) || Character.isSpaceChar(currentChar)) {
                    filteredStringBuilder.append(Character.toUpperCase(currentChar));
                }
            }
            return filteredStringBuilder.toString();
        }
    }


    public static boolean isValidText(String s) {
        boolean isValid = true;
        if (null == s || s.isEmpty() || s.trim().equals("")) {
            isValid = false;
        }
        return isValid;
    }

    /**
     * Method to check whether the edit field has valid text or not
     *
     * @param text
     * @return
     */
    public static boolean isValidField(EditText text) {
        return !(text == null || text.getText() == null || text.getText().toString().trim().length() == 0);
    }


    public static String roundOfFloat(String d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(Float.parseFloat(d)));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.toString();
    }

    public static String replaceLast(String string, String substring, String replacement) {
        int index = string.lastIndexOf(substring);
        if (index == -1)
            return string;
        return string.substring(0, index) + replacement
                + string.substring(index + substring.length());
    }

    public static boolean checkPlayServices(Activity activity) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(activity, resultCode, SBConstants.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }
        return true;
    }


    /**
     * Method to make phone call with given number
     *
     * @param phoneNumber
     */
    public static void call(String phoneNumber, Activity activity) {
        if (activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + phoneNumber));
            activity.startActivity(intent);
        } else {
            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    activity);
            builder.setMessage("As your device does not seem to have call feature, Please call "
                    + phoneNumber + " from another phone");
            builder.setTitle("Call");
            builder.show();
        }
    }

    public static boolean isValidValue(double value, double min, double max) {

        return value >= min && value <= max;
    }


    /**
     * Method to check whether the phone number is valid or not
     *
     * @param number
     * @return
     */
    public static boolean isValidPhone(EditText number) {
        boolean fCheck = Utils.isValidField(number);
        if (fCheck) {
            String data = number.getText().toString().trim();
            if (data.length() < 10 || !PhoneNumberUtils.isGlobalPhoneNumber(data))
                fCheck = false;
        }
        return fCheck;
    }

    public static String encodeParam(String message) {
        if (message == null) {
            return null;
        }
        String encoded = null;
        try {
            encoded = URLEncoder.encode(message, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            SBLogger.showErrorLog("encodeParam", e.toString());
        }
        return encoded;
    }

    /* Method to check whether the email address is valid or not
     *
     * @param emailAddress
     * @return
     */
    public static boolean isValidEmail(String emailAddress) {
        boolean fCheck = Utils.isValidString(emailAddress);
        if (Utils.isValidString(emailAddress)) {
            String data = emailAddress.toString();
            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(data).matches())
                fCheck = false;
        }
        return fCheck;
    }

    public static boolean isValidString(String data) {
        return !(data == null || data == null || data.trim().length() == 0);
    }
}
