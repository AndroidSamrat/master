package com.stockbox.home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.Result;
import com.stockbox.common.SBParentActivity;
import com.stockbox.scanner.view.Scanner_Activity;
import com.stockbox.stockbox.R;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class SB_Main_Activity extends SBParentActivity implements View.OnClickListener{

    private String TAG = "SB_Main_Activity";
    private Button scannow,updatevin;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_sb_main);

        scannow = (Button)findViewById(R.id.scanBtn);
        updatevin = (Button)findViewById(R.id.enterVIN);

        scannow.setOnClickListener(this);
        updatevin.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.scanBtn:
                Intent scannow = new Intent(this,Scanner_Activity.class);
                startActivity(scannow);
                finish();

            break;


            case R.id.enterVIN:


            break;
        }
    }
}
