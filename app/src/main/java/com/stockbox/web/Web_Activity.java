package com.stockbox.web;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.stockbox.appcontroller.SBConstants;
import com.stockbox.common.SBParentActivity;
import com.stockbox.scanner.view.Scanner_Activity;
import com.stockbox.stockbox.R;

public class Web_Activity extends SBParentActivity {

    private String TAG = "Web_Activity";
    private WebView sbWebView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_web);
        sbWebView = (WebView) findViewById(R.id.sbWebView);
        WebSettings webSettings = sbWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        sbWebView.addJavascriptInterface(new ScanButtonClickInterface(this), "STOCKBOX_SCAN");
        sbWebView.loadUrl("file:///android_asset/page.html");

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }


    public void openForScanType1(){
        Intent scannow = new Intent(this,Scanner_Activity.class);
        startActivity(scannow);
        finish();
    }

    public void openForScanType2(){

    }

    public void openForScanType3(){

    }


    public class ScanButtonClickInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        ScanButtonClickInterface(Context c) {
            mContext = c;
        }

        /** Show a toast from the web page */
        @JavascriptInterface
        public void getCode(String type) {
            Toast.makeText(mContext, type, Toast.LENGTH_SHORT).show();

            Intent scannow = new Intent(mContext,Scanner_Activity.class);

            switch (type){
                case "S1":
                    scannow.putExtra("type","s1");
                break;

                case "S2":
                    scannow.putExtra("type","s2");
                break;

                case "S3":
                    scannow.putExtra("type","s3");
                break;
            }
            startActivityForResult(scannow, SBConstants.GET_BARCODE);
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    Bundle res = data.getExtras();
                    String code = res.getString("BAR_CODE");
                    sbWebView.loadUrl("javascript:updateBarCode('"+code+"')");
                }
                break;
        }
    }

}
